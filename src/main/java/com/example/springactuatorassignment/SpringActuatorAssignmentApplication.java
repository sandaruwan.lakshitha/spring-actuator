package com.example.springactuatorassignment;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringActuatorAssignmentApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringActuatorAssignmentApplication.class, args);
	}

}
