package com.example.springactuatorassignment;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class TestController {
    @Value("${runtime.message}")
    private String runTimeMessage;

    @GetMapping("/hello")
    public String helloWorld(){
        return "Hello World";
    }

    @GetMapping("/runtime")
    public String getRunTimeMessage(){
        return runTimeMessage;
    }
}
